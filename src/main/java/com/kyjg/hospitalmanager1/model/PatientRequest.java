package com.kyjg.hospitalmanager1.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PatientRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String patientName;
    @NotNull
    @Length(min = 11, max = 13)
    private String patientPhone;
    @NotNull
    @Length(min = 14, max = 14)
    private String registrationNumber;
    @NotNull
    @Length(min = 2, max = 100)
    private String address;
    @NotNull
    @Length(min = 1)
    private String memo;

}
