package com.kyjg.hospitalmanager1.model;

import com.kyjg.hospitalmanager1.entity.HospitalPatient;
import com.kyjg.hospitalmanager1.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor
public class PatientItem {
    private String patientName;
    private String patientPhone;
    private String registrationNumber;
    private String address;
    private String memo;
    private LocalDate visitFirst;

    private PatientItem(PatientItemBuilder builder) {
        this.patientName = builder.patientName;
        this.patientPhone = builder.patientPhone;
        this.registrationNumber = builder.registrationNumber;
        this.address = builder.address;
        this.memo =builder.memo;
        this.visitFirst = builder.visitFirst;

    }

    public static class PatientItemBuilder implements CommonModelBuilder<PatientItem> {

        private final String patientName;
        private final String patientPhone;
        private final String registrationNumber;
        private final String address;
        private final String memo;
        private final LocalDate visitFirst;

        public PatientItemBuilder(HospitalPatient originData) {
            this.patientName = originData.getPatientName();
            this.patientPhone = originData.getPatientPhone();
            this.registrationNumber = originData.getRegistrationNumber();
            this.address = originData.getAddress();
            this.memo = originData.getMemo();
            this.visitFirst = originData.getVisitFirst();
        }

        @Override
        public PatientItem build() {
            return new PatientItem(this);
        }
    }
}
