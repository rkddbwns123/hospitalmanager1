package com.kyjg.hospitalmanager1.model;

import com.kyjg.hospitalmanager1.entity.ClinicHistory;
import com.kyjg.hospitalmanager1.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryResponse {
    private Long patientId;
    private String patientName;
    private String patientPhone;
    private String registrationNumber;
    private String address;
    private String memo;
    private LocalDate visitFirst;
    private Long historyId;
    private String medicalItem;
    private String isInsurance;
    private Double patientPrice;
    private LocalDate dateClinic;
    private LocalTime timeClinic;
    private String isCalculate;

    private HistoryResponse(HistoryResponseBuilder builder) {
        this.patientId = builder.patientId;
        this.patientName = builder.patientName;
        this.patientPhone = builder.patientPhone;
        this.registrationNumber = builder.registrationNumber;
        this.address = builder.address;
        this.memo = builder.memo;
        this.visitFirst = builder.visitFirst;
        this.historyId = builder.historyId;
        this.medicalItem = builder.medicalItem;
        this.isInsurance = builder.isInsurance;
        this.patientPrice = builder.patientPrice;
        this.dateClinic = builder.dateClinic;
        this.timeClinic = builder.timeClinic;
        this.isCalculate = builder.isCalculate;
    }

    public static class HistoryResponseBuilder implements CommonModelBuilder<HistoryResponse> {

        private final Long patientId;
        private final String patientName;
        private final String patientPhone;
        private final String registrationNumber;
        private final String address;
        private final String memo;
        private final LocalDate visitFirst;
        private final Long historyId;
        private final String medicalItem;
        private final String isInsurance;
        private final Double patientPrice;
        private final LocalDate dateClinic;
        private final LocalTime timeClinic;
        private final String isCalculate;

        public HistoryResponseBuilder(ClinicHistory history) {
            this.patientId = history.getHospitalPatient().getId();
            this.patientName = history.getHospitalPatient().getPatientName();
            this.patientPhone = history.getHospitalPatient().getPatientPhone();
            this.registrationNumber = history.getHospitalPatient().getRegistrationNumber();
            this.address = history.getHospitalPatient().getAddress();
            this.memo = history.getHospitalPatient().getMemo();
            this.visitFirst = history.getHospitalPatient().getVisitFirst();
            this.historyId = history.getId();
            this.medicalItem = history.getMedicalItem().getName();
            this.isInsurance = history.getIsInsurance() ? "급여" : "비급여";
            this.patientPrice = history.getPatientPrice();
            this.dateClinic = history.getDateClinic();
            this.timeClinic = history.getTimeClinic();
            this.isCalculate = history.getIsCalculate() ? "완료" : "대기";
        }
        @Override
        public HistoryResponse build() {
            return new HistoryResponse(this);
        }
    }
}
