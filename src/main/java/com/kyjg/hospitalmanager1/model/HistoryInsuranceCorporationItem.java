package com.kyjg.hospitalmanager1.model;

import com.kyjg.hospitalmanager1.entity.ClinicHistory;
import com.kyjg.hospitalmanager1.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryInsuranceCorporationItem {
    private String patientName;
    private String patientPhone;
    private String registrationNumber;
    private String medicalItem;
    private Double nonInsurancePrice;
    private Double patientPrice;
    private Double billingAmount;
    private LocalDate dateClinic;

    private HistoryInsuranceCorporationItem(HistoryInsuranceCorporationItemBuilder builder) {
        this.patientName = builder.patientName;
        this.patientPhone = builder.patientPhone;
        this.registrationNumber = builder.registrationNumber;
        this.medicalItem = builder.medicalItem;
        this.nonInsurancePrice = builder.nonInsurancePrice;
        this.patientPrice = builder.patientPrice;
        this.billingAmount = builder.billingAmount;
        this.dateClinic = builder.dateClinic;
    }

    public static class HistoryInsuranceCorporationItemBuilder implements CommonModelBuilder<HistoryInsuranceCorporationItem> {

        private final String patientName;
        private final String patientPhone;
        private final String registrationNumber;
        private final String medicalItem;
        private final Double nonInsurancePrice;
        private final Double patientPrice;
        private final Double billingAmount;
        private final LocalDate dateClinic;

        public HistoryInsuranceCorporationItemBuilder(ClinicHistory history) {
            this.patientName = history.getHospitalPatient().getPatientName();
            this.patientPhone = history.getHospitalPatient().getPatientPhone();
            this.registrationNumber = history.getHospitalPatient().getRegistrationNumber();
            this.medicalItem = history.getMedicalItem().getName();
            this.nonInsurancePrice = history.getMedicalItem().getNonInsurancePrice();
            this.patientPrice = history.getPatientPrice();
            this.billingAmount = history.getMedicalItem().getNonInsurancePrice() - history.getPatientPrice();
            this.dateClinic = history.getDateClinic();
        }

        @Override
        public HistoryInsuranceCorporationItem build() {
            return new HistoryInsuranceCorporationItem(this);
        }
    }
}
