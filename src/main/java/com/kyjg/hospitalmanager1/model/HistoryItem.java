package com.kyjg.hospitalmanager1.model;

import com.kyjg.hospitalmanager1.entity.ClinicHistory;
import com.kyjg.hospitalmanager1.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryItem {
    private Long historyId;
    private String patientName;
    private String patientPhone;
    private String registrationNumber;
    private String isInsurance;
    private Double patientPrice;
    private String medicalItem;
    private LocalDate dateClinic;
    private LocalTime timeClinic;
    private String isCalculate;

    private HistoryItem (HistoryItemBuilder builder) {
        this.historyId = builder.historyId;
        this.patientName = builder.patientName;
        this.patientPhone = builder.patientPhone;
        this.registrationNumber = builder.registrationNumber;
        this.isInsurance = builder.isInsurance;
        this.patientPrice = builder.patientPrice;
        this.medicalItem = builder.medicalItem;
        this.dateClinic = builder.dateClinic;
        this.timeClinic = builder.timeClinic;
        this.isCalculate = builder.isCalculate;
    }

    public static class HistoryItemBuilder implements CommonModelBuilder<HistoryItem> {
        private final Long historyId;
        private final String patientName;
        private final String patientPhone;
        private final String registrationNumber;
        private final String isInsurance;
        private final Double patientPrice;
        private final String medicalItem;
        private final LocalDate dateClinic;
        private final LocalTime timeClinic;
        private final String isCalculate;

        public HistoryItemBuilder(ClinicHistory history) {
            this.historyId = history.getId();
            this.patientName = history.getHospitalPatient().getPatientName();
            this.patientPhone = history.getHospitalPatient().getPatientPhone();
            this.registrationNumber = history.getHospitalPatient().getRegistrationNumber();
            this.isInsurance = history.getIsInsurance() ? "급여" : "비급여";
            this.patientPrice = history.getPatientPrice();
            this.medicalItem = history.getMedicalItem().getName();
            this.dateClinic = history.getDateClinic();
            this.timeClinic = history.getTimeClinic();
            this.isCalculate = history.getIsCalculate() ? "완료" : "대기";
        }


        @Override
        public HistoryItem build() {
            return new HistoryItem(this);
        }
    }
}
