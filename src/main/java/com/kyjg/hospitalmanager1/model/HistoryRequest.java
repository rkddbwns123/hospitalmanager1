package com.kyjg.hospitalmanager1.model;

import com.kyjg.hospitalmanager1.enums.MedicalItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HistoryRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;
    @NotNull
    private Boolean isInsurance;
}
