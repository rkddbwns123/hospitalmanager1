package com.kyjg.hospitalmanager1.service;

import com.kyjg.hospitalmanager1.entity.HospitalPatient;
import com.kyjg.hospitalmanager1.model.PatientInfoUpdateRequest;
import com.kyjg.hospitalmanager1.model.PatientItem;
import com.kyjg.hospitalmanager1.model.PatientRequest;
import com.kyjg.hospitalmanager1.repository.HospitalPatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PatientService {
    private final HospitalPatientRepository hospitalPatientRepository;

    public void setPatient(PatientRequest request) {
        HospitalPatient addData = new HospitalPatient.HospitalPatientBuilder(request).build();

        hospitalPatientRepository.save(addData);
    }

    public HospitalPatient getData(long id) {

        return hospitalPatientRepository.findById(id).orElseThrow();
    }

    public List<PatientItem> getPatient() {
        List<HospitalPatient> originList = hospitalPatientRepository.findAll();

        List<PatientItem> result = new LinkedList<>();
        originList.forEach(item -> result.add(new PatientItem.PatientItemBuilder(item).build()));

        return result;
    }

    public void putPatientInfo(long id, PatientInfoUpdateRequest request) {
        HospitalPatient originData = hospitalPatientRepository.findById(id).orElseThrow();
        originData.putInfoUpdate(request);

        hospitalPatientRepository.save(originData);
    }
}
