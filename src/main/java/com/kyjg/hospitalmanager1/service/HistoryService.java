package com.kyjg.hospitalmanager1.service;

import com.kyjg.hospitalmanager1.entity.ClinicHistory;
import com.kyjg.hospitalmanager1.entity.HospitalPatient;
import com.kyjg.hospitalmanager1.model.HistoryInsuranceCorporationItem;
import com.kyjg.hospitalmanager1.model.HistoryItem;
import com.kyjg.hospitalmanager1.model.HistoryRequest;
import com.kyjg.hospitalmanager1.model.HistoryResponse;
import com.kyjg.hospitalmanager1.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalPatient hospitalPatient, HistoryRequest request) {
        ClinicHistory addData = new ClinicHistory.ClinicHistoryBuilder(hospitalPatient, request).build();

        clinicHistoryRepository.save(addData);
    }

    public List<HistoryItem> getHistoriesByDate(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByDateClinicOrderByIdDesc(searchDate);

        List<HistoryItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new HistoryItem.HistoryItemBuilder(item).build()));

        return result;
    }
    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByIsInsuranceAndDateClinicAndIsCalculateOrderByIdDesc(true, searchDate, true);

        List<HistoryInsuranceCorporationItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new HistoryInsuranceCorporationItem.HistoryInsuranceCorporationItemBuilder(item).build()));
        return result;
    }
    public HistoryResponse getHistory(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();

        HistoryResponse result = new HistoryResponse.HistoryResponseBuilder(originData).build();

        return result;
    }
    public void putCalculate(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();
        originData.putCalculate();

        clinicHistoryRepository.save(originData);
    }
}
