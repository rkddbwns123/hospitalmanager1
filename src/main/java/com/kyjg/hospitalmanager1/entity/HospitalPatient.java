package com.kyjg.hospitalmanager1.entity;

import com.kyjg.hospitalmanager1.interfaces.CommonModelBuilder;
import com.kyjg.hospitalmanager1.model.PatientInfoUpdateRequest;
import com.kyjg.hospitalmanager1.model.PatientRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HospitalPatient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String patientName;
    @Column(nullable = false, length = 13)
    private String patientPhone;
    @Column(nullable = false, length = 14)
    private String registrationNumber;
    @Column(nullable = false, length = 100)
    private String address;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String memo;
    @Column(nullable = false)
    private LocalDate visitFirst;

    public void putInfoUpdate(PatientInfoUpdateRequest request) {
        this.patientName = request.getPatientName();
        this.patientPhone = request.getPatientPhone();
        this.address = request.getAddress();
        this.memo = request.getMemo();

    }

    private  HospitalPatient(HospitalPatientBuilder builder) {
        this.patientName = builder.patientName;
        this.patientPhone = builder.patientPhone;
        this.registrationNumber = builder.registrationNumber;
        this.address = builder.address;
        this.memo = builder.memo;
        this.visitFirst = builder.visitFirst;
    }

    public static class HospitalPatientBuilder implements CommonModelBuilder<HospitalPatient> {
        private final String patientName;
        private final String patientPhone;
        private final String registrationNumber;
        private final String address;
        private final String memo;
        private final LocalDate visitFirst;

        public HospitalPatientBuilder(PatientRequest request) {
            this.patientName = request.getPatientName();
            this.patientPhone = request.getPatientPhone();
            this.registrationNumber = request.getRegistrationNumber();
            this.address = request.getAddress();
            this.memo = request.getMemo();
            this.visitFirst = LocalDate.now();
        }

        @Override
        public HospitalPatient build() {
            return new HospitalPatient(this);
        }
    }
}
