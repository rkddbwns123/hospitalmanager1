package com.kyjg.hospitalmanager1.entity;

import com.kyjg.hospitalmanager1.enums.MedicalItem;
import com.kyjg.hospitalmanager1.interfaces.CommonModelBuilder;
import com.kyjg.hospitalmanager1.model.HistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ClinicHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patientId", nullable = false)
    private HospitalPatient hospitalPatient;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;
    @Column(nullable = false)
    private Boolean isInsurance;
    @Column(nullable = false)
    private Double patientPrice;
    @Column(nullable = false)
    private LocalDate dateClinic;
    @Column(nullable = false)
    private LocalTime timeClinic;
    @Column(nullable = false)
    private Boolean isCalculate;

    public void putCalculate() {
        this.isCalculate = true;
    }

    private ClinicHistory(ClinicHistoryBuilder builder) {
        this.hospitalPatient = builder.hospitalPatient;
        this.medicalItem = builder.medicalItem;
        this.isInsurance = builder.isInsurance;
        this.patientPrice = builder.patientPrice;
        this.dateClinic = builder.dateClinic;
        this.timeClinic = builder.timeClinic;
        this.isCalculate = builder.isCalculate;

    }

    public static class ClinicHistoryBuilder implements CommonModelBuilder<ClinicHistory> {

        private final HospitalPatient hospitalPatient;
        private final MedicalItem medicalItem;
        private final Boolean isInsurance;
        private final Double patientPrice;
        private final LocalDate dateClinic;
        private final LocalTime timeClinic;
        private final Boolean isCalculate;

        public ClinicHistoryBuilder(HospitalPatient hospitalPatient, HistoryRequest request) {
            this.hospitalPatient = hospitalPatient;
            this.medicalItem = request.getMedicalItem();
            this.isInsurance = request.getIsInsurance();
            this.patientPrice = request.getIsInsurance() ? request.getMedicalItem().getInsurancePrice() : request.getMedicalItem().getNonInsurancePrice();
            this.dateClinic = LocalDate.now();
            this.timeClinic = LocalTime.now();
            this.isCalculate = false;
        }

        @Override
        public ClinicHistory build() {
            return new ClinicHistory(this);
        }
    }
}
