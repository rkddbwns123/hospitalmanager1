package com.kyjg.hospitalmanager1.controller;

import com.kyjg.hospitalmanager1.entity.HospitalPatient;
import com.kyjg.hospitalmanager1.model.HistoryInsuranceCorporationItem;
import com.kyjg.hospitalmanager1.model.HistoryItem;
import com.kyjg.hospitalmanager1.model.HistoryRequest;
import com.kyjg.hospitalmanager1.model.HistoryResponse;
import com.kyjg.hospitalmanager1.service.HistoryService;
import com.kyjg.hospitalmanager1.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final HistoryService historyService;

    private final PatientService patientService;

    @PostMapping("/data/patient-id/{patientId}")
    public String setHistory(@PathVariable long patientId, @RequestBody @Valid HistoryRequest request) {
        HospitalPatient hospitalPatient = patientService.getData(patientId);
        historyService.setHistory(hospitalPatient, request);

        return "OK";
    }
    @GetMapping("/all/date/")
    public List<HistoryItem> getHistoriesByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate searchDate) {
        return historyService.getHistoriesByDate(searchDate);
    }
    @GetMapping("/all/insurance")
    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate searchDate) {
        return historyService.getHistoriesByInsurance(searchDate);
    }
    @GetMapping("/all/history-id/{historyId}")
    public HistoryResponse getHistory(@PathVariable long historyId) {
        return historyService.getHistory(historyId);
    }
    @PutMapping("/calculate/id/{id}")
    public String putCalculate(@PathVariable long id) {
        historyService.putCalculate(id);

        return "OK";
    }
}
