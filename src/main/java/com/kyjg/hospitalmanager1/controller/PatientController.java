package com.kyjg.hospitalmanager1.controller;

import com.kyjg.hospitalmanager1.entity.HospitalPatient;
import com.kyjg.hospitalmanager1.model.PatientInfoUpdateRequest;
import com.kyjg.hospitalmanager1.model.PatientItem;
import com.kyjg.hospitalmanager1.model.PatientRequest;
import com.kyjg.hospitalmanager1.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/patient")
public class PatientController {
    private final PatientService patientService;
    @PostMapping("/data")
    public String setPatient(@RequestBody @Valid PatientRequest request) {
        patientService.setPatient(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<PatientItem> getPatient() {
        List<PatientItem> result = patientService.getPatient();

        return result;
    }

    @PutMapping("/update-info/id/{id}")
    public String putPatientInfo(@PathVariable long id, @RequestBody @Valid PatientInfoUpdateRequest request) {
        patientService.putPatientInfo(id, request);

        return "OK";
    }
}
