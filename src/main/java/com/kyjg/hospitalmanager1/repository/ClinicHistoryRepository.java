package com.kyjg.hospitalmanager1.repository;

import com.kyjg.hospitalmanager1.entity.ClinicHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ClinicHistoryRepository extends JpaRepository<ClinicHistory, Long> {
    List<ClinicHistory> findAllByDateClinicOrderByIdDesc(LocalDate searchDate);
    List<ClinicHistory> findAllByIsInsuranceAndDateClinicAndIsCalculateOrderByIdDesc(boolean isInsurance, LocalDate searchDate, boolean isCalculate);
}
