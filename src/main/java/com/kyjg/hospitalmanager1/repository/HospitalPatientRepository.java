package com.kyjg.hospitalmanager1.repository;

import com.kyjg.hospitalmanager1.entity.HospitalPatient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HospitalPatientRepository extends JpaRepository<HospitalPatient, Long> {
}
