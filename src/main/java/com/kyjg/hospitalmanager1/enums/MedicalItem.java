package com.kyjg.hospitalmanager1.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MedicalItem {
    CHUNA("추나치료", 50000, 20000),
    PHYSICS("물리치료", 30000, 5000),
    ACUPUNCTURE("침치료", 10000, 3000);

    private final String name;

    private final double nonInsurancePrice;

    private final double insurancePrice;
}
