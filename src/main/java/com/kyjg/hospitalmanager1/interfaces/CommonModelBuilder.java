package com.kyjg.hospitalmanager1.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
